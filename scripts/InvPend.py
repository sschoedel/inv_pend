import FaBo9Axis_MPU9250
import RPi.GPIO as GPIO
import math
import sys
import time

''' For later
import plotly.graph_objects as go
import numpy as np

fig = go.Figure()

pSteps = []
for i in np.arange(0, 10, .1):
    pStep = dict(
        method="restyle",
        args=["visible", [False] * 10],
    )
    pStep["args"][1][int(i)] = True
    pSteps.append(pStep)
    
sliders = [dict(
    active=10,
    currentvalue={"prefix": "Proportional: "},
    pad={"t": 50},
    steps=pSteps
)]

fig.update_layout(sliders=sliders)
fig.show()
'''
#setup mpu
mpu = FaBo9Axis_MPU9250.MPU9250()
def setupGPIO():
    GPIO.setmode(GPIO.BCM)
    
    #pin defs
    mLin1 = 5
    mLin2 = 0
    mRin1 = 6
    mRin2 = 13
    mLpinpwm = 19
    mRpinpwm = 26
    
    # setup motor pwm pins
    GPIO.setup(mLpinpwm, GPIO.OUT)
    GPIO.setup(mRpinpwm, GPIO.OUT)

    # setup motor direction pins
    GPIO.setup(mLin1, GPIO.OUT)
    GPIO.setup(mLin2, GPIO.OUT)
    GPIO.setup(mRin1, GPIO.OUT)
    GPIO.setup(mRin2, GPIO.OUT)
    
    return mLin1, mLin2, mRin1, mRin2, mLpinpwm, mRpinpwm

''' Leaving this here in case we want to try using RTIMU ever again
def setupRTIMU():
    #create IMU objects
    SETTINGS_FILE = "RTIMULib"
    s = RTIMU.Settings(SETTINGS_FILE)
    imu = RTIMU.RTIMU(s)

    if not imu.IMUInit():
        print("im out")
        sys.exit()
        
    # setup IMU
    imu.setSlerpPower(0.02)
    imu.setGyroEnable(True)
    imu.setAccelEnable(True)
    imu.setCompassEnable(True)
    
    # poll IMU
    poll_interval = imu.IMUGetPollInterval()
    
    return imu, poll_interval
'''
    
# change directions
def setForward(*ms):
    for motor in range(0, len(ms)):
        GPIO.output(ms[motor][0], 1)
        GPIO.output(ms[motor][1], 0)
    
    
def setBack(*ms):
    for motor in range(0, len(ms)):
        GPIO.output(ms[motor][0], 0)
        GPIO.output(ms[motor][1], 1)

def calibrate():
    accel = mpu.readAccel()
    print("Rotate tim back and forth a bit please")
    print("Press escape when ready")
    maxVal = 0
    try:
        if accel['z'] > maxVal:
            maxVal = accel['z']
    except KeyboardInterrupt:
        pass
    return maxVal

mLin1, mLin2, mRin1, mRin2, mLpinpwm, mRpinpwm = setupGPIO()
#imu, poll_interval = setupRTIMU()


# array of pins to control motor directions
mLs = [mLin1, mLin2]
mRs = [mRin1, mRin2]

# setup pwm objs
mLpwm = GPIO.PWM(mLpinpwm, 200)
mRpwm = GPIO.PWM(mRpinpwm, 200)

mLpwm.start(0)
mRpwm.start(0)

#constants
Pweight = 1
Iweight = 0
Dweight = 0

maxValue = 0.68
# maxValue = calibrate()
# Left motor tends to start moving around PWM of 20
# Right motor around 30

accel = mpu.readAccel()
gyroDiff = .05
accelDiff = 0.03

forward = False
back = False

accelPartL = 0
accelPartR = 0

prevAngErr = abs(maxValue - abs(accel['z']))

falseAccelChange = .1

prevTime = time.time()
def calibrate():
    maxAccel = 0
    currentAccelZ = 0
    value = raw_input("Press enter to begin calibration")
    print("Tilt timmy back and forth for five seconds")
    while time.time() - prevTime < 5:
        accel = mpu.readAccel()
        
        currentAccelZ = abs(accel['z'])
        if currentAccelZ > maxAccel:
            maxAccel = currentAccelZ
        print("".format())
        print("Current accel: {}; Max accel: {}; Time left: {}".format(currentAccelZ, maxAccel, int(6 + prevTime - time.time())))
    return maxAccel

    
maxAccel = calibrate()
goalAngArea = 0.1
deadZone = 1
accelDiff = 0.01
diff = 2
angleXtot = 0
timeFix = 0.0000001
gyroFix = -0.1
gyroFixAdd = 0.00001

forwardComp = 0
forwardCompAdd = 0.0000001
backComp = 0
backCompAdd = 0.0000001

goalAng = 0
goalAngChange = 0.05

prevBack = False
prevForward = False

thetaGyro = 0
thetaAccel = 0

stableTime = time.time()


try:
    while(1):
        # print("Proportional: {}".format(pStep)) # for later
        # read and print IMU data
        accel = mpu.readAccel()
        gyro = mpu.readGyro()
        mag = mpu.readMagnet()
        
        rawAccel = accel['z']
        if abs(rawAccel) > maxAccel:
            rawAccel = maxAccel
        prevTime = time.time()
        thetaAccel = (172.5 - (math.acos(rawAccel/maxAccel) * 180 / math.pi))
        thetaGyro = thetaGyro + ((time.time() - prevTime) * gyro['y']) * (180/math.pi)
                
        # alpha related to gyro data - alpha must be smaller than 1
        # assuming thetaGyro never goes above 150 - 
        # change this value to get proper relationship between gyro and accel thetas       
        if thetaGyro < 0:
            direction = -1
        else:
            direction = 1
        
        if abs(gyro['y']) > 150:
            gyroAlpha = 150
        else:
            gyroAlpha = abs(gyro['y'])
        alpha = gyroAlpha/150
        
        if alpha > .1:
            stableTime = time.time()
        elif time.time() - stableTime > 0.1 and alpha < 15:
            stableTime = time.time()
            thetaGyro = thetaAccel * direction
        
        totalTheta = alpha * thetaGyro + (1-alpha) * thetaAccel * direction
        
        if totalTheta < 0:
            setBack(mLs, mRs)
        else:
            setForward(mLs, mRs)
        '''
        accel = abs(accel['z'])
        gyroY = gyro['y']# + gyroFix
        #gyroFix = gyroFix + gyroFixAdd
        
        angErr = maxValue - accel
        angErrDiff = abs(prevAngErr - angErr)
        
        # ignore small gyro changes
        #if abs(gyroY) < diff:
        #    gyroY = 0
        
        #angleXtot = angleXtot + .98 * ((time.time() - prevTime) * gyroY) + .02 * (accel * (time.time() - prevTime))
        angleXtot = angleXtot + (time.time() - prevTime) * gyroY + gyroFixAdd + 1
        prevTime = time.time()
        
        # set direction based on angle from vertical
        if angleXtot < -(goalAng + deadZone):
            setBack(mLs, mRs)
            prevBack = True
            prevForward = False
            #angleXtot = angleXtot - backComp
            #backComp = backComp - backCompAdd
        elif angleXtot > (goalAng + deadZone):
            setForward(mLs, mRs)
            prevBack = False
            prevForward = True
            #angleXtot = angleXtot + forwardComp
            #forwardComp = forwardComp + forwardCompAdd
        else:
            prevBack = False
            prevForward = False
            
        if abs(abs(angleXtot) - abs(goalAng)) < goalAngArea:
            if gyroY < 0:
                goalAng = goalAng + goalAngChange
            elif gyroY > 0:
                goalAng = goalAng - goalAngChange
                '''
        
        # reset angle if vertical
        #if abs(gyroY) < diff and abs(angErr) < accelDiff:
            # reset angle if vertical
            #angleXtot = 0
            #backComp = 0  # reset compensation values
            #forwardComp = 0
            
        # this bit of code uses accel data
        ''' 
        if angErrDiff > falseAccelChange:
            angErr = prevAngErr
        else:
            prevAngErr = angErr
        
        # Change wheel direction based on which way the robot is leaning
        if not forward and not back: # not falling
            if angErr > accelDiff and gyro['y'] > 0: # starting to fall forward
                forward = True
                setForward(mLs, mRs)
                fallDirection = "Forward"
            elif angErr > accelDiff and gyro['y'] < 0: # starting to fall back
                back = True
                setBack(mLs, mRs)
                fallDirection = "Back"
            else:
                accelPartL = 0
                accelPartR = 0
                fallDirection = "Stable"
        else:
            accelPartL = (maxValue - accel) * Pweight + 30
            accelPartR = (maxValue - accel) * Pweight + 20
        
        if angErr < accelDiff: # set to not falling
            forward = False
            back = False
            accelPart = 0
            fallDirection = "Stable"
        '''
        # sum values for pid
        dcL = abs(totalTheta) * Pweight # add i and d after this
        dcR = abs(totalTheta) * Pweight
        
        # ChangeDutyCycle can't accept any value over 100
        if abs(dcL) > 100:
            dcL = 100
        if abs(dcR) > 100:
            dcR = 100
        #print("Fall direction: {:^7}; DC value L, R: {:^3}, {:^3}; angErr: {}; err diff {}".format(fallDirection, dcL, dcR, angErr, angErrDiff))
        print("angleXtot: {:^6}; gyro {}; DCr, DCl: {:^3}, {:^3}".format(angleXtot, gyro['y'], dcR, dcL))
        print("accel: {}; ".format(accel))
        
        mLpwm.ChangeDutyCycle(abs(dcL))
        mRpwm.ChangeDutyCycle(abs(dcR))
        
except KeyboardInterrupt:
    print("Except reached")
    mLpwm.ChangeDutyCycle(0)
    mRpwm.ChangeDutyCycle(0)
GPIO.cleanup()
print("cleaned")
sys.exit()
    


