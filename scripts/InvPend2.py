'''
This only really exists to debug IMU output data.
'''

import FaBo9Axis_MPU9250
import time
import sys
import math

mpu = FaBo9Axis_MPU9250.MPU9250()
prevTime = time.time()

def calibrate():
    maxAccel = 0
    currentAccelZ = 0
    value = raw_input("Press enter to begin calibration")
    print("Tilt timmy back and forth for five seconds")
    while time.time() - prevTime < 5:
        accel = mpu.readAccel()
        
        currentAccelZ = abs(accel['z'])
        if currentAccelZ > maxAccel:
            maxAccel = currentAccelZ
        print("".format())
        print("Current accel: {}; Max accel: {}; Time left: {}".format(currentAccelZ, maxAccel, int(6 + prevTime - time.time())))
    return maxAccel

    
maxAccel = calibrate()

thetaGyro = 0
thetaGyro2 = 0
prevTheta = 0
prevAccel = 0

stableTime = time.time()

accelDiff1 = 0.02
accelDiff2 = 0.005
forward = False
back = False
negate = -1
reset = False
direction = 1
try:
    while True:        
        accel = mpu.readAccel()
        gyro = mpu.readGyro()
        mag = mpu.readMagnet()
        
        rawAccel = accel['z']
        if abs(rawAccel) > maxAccel:
            rawAccel = maxAccel
        prevTime = time.time()
        thetaAccel = (172.5 - (math.acos(rawAccel/maxAccel) * 180 / math.pi))
        thetaGyro = thetaGyro + ((time.time() - prevTime) * gyro['y']) * (180/math.pi)
                
        # alpha related to gyro data - alpha must be smaller than 1
        # assuming thetaGyro never goes above 150 - 
        # change this value to get proper relationship between gyro and accel thetas       
        if thetaGyro < 0:
            direction = -1
        else:
			direction = 1
        
        if abs(gyro['y']) > 150:
            gyroAlpha = 150
        else:
            gyroAlpha = abs(gyro['y'])
        alpha = gyroAlpha/150
        
        if alpha > .1:
            stableTime = time.time()
        elif time.time() - stableTime > 0.1 and alpha < 15:
            stableTime = time.time()
            thetaGyro = thetaAccel * direction
        
        totalTheta = alpha * thetaGyro + (1-alpha) * thetaAccel * direction
        # when alpha is large (gyro is large), most of theta is from gyro
        # when alpha is small (gyro is small), most of theta is from accel
        
        # if close to vertical and accel hasn't changed too much from past cycle
        '''if maxAccel - rawAccel < accelDiff1 and abs(prevAccel - rawAccel) < accelDiff2:
            if reset is False:
                stableTime = time.time()
                reset = True
            
            if gyro['y'] < 0 and time.time() - stableTime > 0.1:
                reset = False
                direction = -1
            elif gyro['y'] > 0 and time.time() - stableTime > 0.1:
                reset = False
                direction = 1'''
        
        prevAccel = rawAccel
        
        print("thetaAccel: {:^3}; thetaGyro {}; totalTheta {}".format(thetaAccel, thetaGyro, totalTheta))
		
            
except KeyboardInterrupt:
    sys.exit()
    
