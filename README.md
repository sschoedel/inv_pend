Code, models, and resources for an inverted pendulum robot.


Resources used in building this robot:

[Inverted Pendulum Wiki](https://en.wikipedia.org/wiki/Inverted_pendulum)

[Brian Douglas YouTube page](https://www.youtube.com/user/ControlLectures/playlists)
